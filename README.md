# XLiveLessNess (XLLN)
Games for Windows LiveLessNess. A complete Games For Windows - LIVE<sup>TM</sup> (GFWL) rewrite.

# XLLN-Data
This repository contains various files containing data and settings that assists in the functionality of [XLiveLessNess](https://gitlab.com/GlitchyScripts/xlivelessness). Typically in this repository you will find default profile settings, Title names and versions, achievement data and images and perhaps marketplace data or more in the future.

## License

Copyright (C) 2024 Glitchy Scripts

This library is free software; you can redistribute it and/or
modify it under the terms of exclusively the GNU Lesser General Public
License version 2.1 as published by the Free Software Foundation.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License version 2.1 along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
