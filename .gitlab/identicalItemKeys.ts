import type { Plugin, AnySchemaObject } from "ajv";

const identicalItemKeys: Plugin<undefined> = (ajv) =>
  ajv.addKeyword({
    keyword: "identicalItemKeys",
    type: "array",
    schemaType: "array",
    compile(keys: string[], _: AnySchemaObject) {
      return (data) => {
        if (data.length <= 1) return true;
        for (const key of keys) {
          const dk = data.map((d) => Object.keys(d[key]));
          if (dk.length <= 1) continue;
          for (const v of dk) {
            if (
              v.length !== dk[0].length ||
              !v.every((x) => dk[0].includes(x))
            ) {
              return false;
            }
          }
        }
        return true;
      };
    },
    metaSchema: {
      type: "array",
      items: { type: "string" },
    },
  });

export default identicalItemKeys;
module.exports = identicalItemKeys;
