import Ajv2020 from "ajv/dist/2020";
import { existsSync, readdirSync } from "fs";
import { join } from "path";

var errors = 0;

const ajv = new Ajv2020();
require("ajv-keywords/dist/keywords/uniqueItemProperties")(ajv);
require("./identicalItemKeys")(ajv);

const achievements_schema = require("./achievements.schema.json");
const validate_achievements = ajv.compile(achievements_schema);

const achievements = "../XLiveLessNess/achievements";
const dirs = readdirSync(achievements, { withFileTypes: true })
  .filter((dirent) => dirent.isDirectory())
  .map((dirent) => dirent.name);

for (const dir of dirs) {
  // Validate details.json with schema
  const details = require(join(achievements, dir, "details.json"));
  if (!validate_achievements(details)) {
    console.error(
      `Validation failed for ${dir}/details.json: ${ajv.errorsText(
        validate_achievements.errors
      )}`
    );
    errors++;
    continue;
  }

  // Check that the achievement icons exist
  for (const achievement of details) {
    if (!existsSync(join(achievements, dir, `${achievement.dwImageId}.png`))) {
      console.error(`Missing icon for ${dir}/${achievement.dwImageId}.png`);
      errors++;
    }
  }
}

const titles_schema = require("./titles.schema.json");
const validate_titles = ajv.compile(titles_schema);

const titles = require("../XLiveLessNess/hub/titles.json");
if (!validate_titles(titles)) {
  console.error(
    `Validation failed for titles.json: ${ajv.errorsText(
      validate_titles.errors
    )}`
  );
  errors++;
}

process.exit(errors);
